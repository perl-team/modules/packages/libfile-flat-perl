libfile-flat-perl (1.07-1) unstable; urgency=medium

  * Import upstream version 1.07.
  * Update Upstream-Contact.

 -- gregor herrmann <gregoa@debian.org>  Tue, 25 Aug 2020 02:17:25 +0200

libfile-flat-perl (1.06-1) unstable; urgency=medium

  * debian/watch: use uscan version 4.
  * Import upstream version 1.06.
  * Update years of packaging copyright.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Tue, 04 Aug 2020 00:48:41 +0200

libfile-flat-perl (1.05-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian spelling-error-in-description warning

  [ gregor herrmann ]
  * New upstream release.
  * Add debian/upstream/metadata.
  * Update debian/copyright.
    Change Upstream-Contact, update years, remove information about
    dropped files.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.3.0.
  * Bump debhelper compatibility level to 11.
  * Remove trailing whitespace from debian/*.
  * Add /me to Uploaders.
  * Install new upstream CONTRIBUTING document.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Jan 2019 18:54:15 +0100

libfile-flat-perl (1.04-5) unstable; urgency=low

  [ Jonas Genannt ]
  * Changed Maintainer to Perl Group
  * d/control:
    - added VCS URLs
    - bumped standards version to 3.9.5 (no changes needed)
    - updated short description
  * d/copyright: updated year of copyright

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Wed, 13 Aug 2014 10:44:03 +0200

libfile-flat-perl (1.04-4) unstable; urgency=low

  * d/control:
   - removed DM Upload Allow
   - bumped standards/compat version
  * Changed URLs from cpan to MetaCpan
  * d/copyright: update to 1.0

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Sun, 22 Sep 2013 13:17:21 +0200

libfile-flat-perl (1.04-3) unstable; urgency=low

  * Switch to dpkg-source 3.0 (quilt) format
  * bumped debhelper compat version
  * bumped standards version
  * d/watch: added watch file
  * d/control: added Homepage field
  * d/control: added misc:Depends to Depends
  * d/rules: reduced
  * d/copyright: changed to DEP5
  * d/control: Added DM-Upload-Allow

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Mon, 09 Jan 2012 15:40:26 +0100

libfile-flat-perl (1.04-2) unstable; urgency=low

  * Added Build-Depend: libparams-util-perl (Closes: #527820)
  * Added Depend: libparams-util-perl
  * new standards version
  * new debhelper version (7)

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Fri, 08 May 2009 23:35:23 +0200

libfile-flat-perl (1.04-1) unstable; urgency=low

  * New Upstream Release
  * new standards version

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Wed, 24 Sep 2008 20:04:45 +0200

libfile-flat-perl (1.00-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS with Perl 5.10.  Closes: #467793

 -- Mark Hymers <mhy@debian.org>  Sat, 05 Apr 2008 21:08:59 +0100

libfile-flat-perl (1.00-1) unstable; urgency=low

  * New Upstream Release
  * bumped Standards-Version to 3.7.2 (no changes needed)
  * Added Build-Depend: libfile-copy-recursive-perl (for make test)
  * Added Depend: libfile-copy-recursive-perl

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Fri, 9 Feb 2007 14:19:34 +0100

libfile-flat-perl (0.95-2) unstable; urgency=low

  * debian/copyright: fixed Module name

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Thu, 12 Jan 2006 20:17:51 +0000

libfile-flat-perl (0.95-1) unstable; urgency=low

  * Initial Release (Closes: #346324)

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Sat,  7 Jan 2006 19:15:34 +0200
